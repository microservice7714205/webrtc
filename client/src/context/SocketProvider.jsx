import React, { createContext, useMemo, useContext } from "react";
import { io } from "socket.io-client";

const SocketContext = createContext(null);

export const useSocket = () => {
  const socket = useContext(SocketContext);
  return socket;
};

// let host = "192.168.9.57"
let host = "13.126.124.31";

export const SocketProvider = (props) => {
  const socket = useMemo(() => io(`${host}:8000`), []);

  return (
    <SocketContext.Provider value={socket}>
      {props.children}
    </SocketContext.Provider>
  );
};
